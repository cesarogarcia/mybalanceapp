<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;

class notes extends TestCase
{
    protected function setUp(): void// tabla ficticia de DDBB
    {
        parent::setUp();
        DB::unprepared("
            CREATE TABLE notes (
                id	TEXT NOT NULL,
                type	TEXT NOT NULL,
                amount	TEXT NOT NULL,
                date	TEXT NOT NULL,
                category	TEXT NOT NULL,
                description	TEXT NOT NULL,
                PRIMARY KEY(id)
           ); 
            INSERT INTO notes VALUES ('1','income', '1', '1', 'category 1','description 1');
            INSERT INTO notes VALUES ('2','expenses', '2', '2', 'category 2','description 2');
            INSERT INTO notes VALUES ('3','expenses', '2', '1', 'category 2','description 2');
        ");
        //$this->withoutExceptionHandling();
    }

    //test de consulta de movimientos
    public function testGetMovements()
    {
        $this->json('GET', 'api/notes')
            // assertStatus comprueba es status code de la respuesta
            ->assertStatus(200)
            // assertJson comprueba que el objeto JSON que se devuelve es compatible
            // con lo que se espera. 
            // También existe assertExactJson para comprobaciones exactas
            // https://laravel.com/docs/5.8/http-tests#testing-json-apis
            ->assertJson([
                [                   
                    'id' => '2',
                    'type' => 'expenses',
                    'amount' => '2',
                    'date' => '2',
                    'category' => 'category 2',
                    'description' => 'description 2',

                ],
                [  'id' => '1',
                    'type' => 'income',
                    'amount' => '1',
                    'date' => '1',
                    'category' => 'category 1',
                    'description' => 'description 1', 
                ],
                [
                    'id' => '3',
                    'type' => 'expenses',
                    'amount' => '2',
                    'date' => '1',
                    'category' => 'category 2',
                    'description' => 'description 2',
                ],
            ]);
    }
 
    //test de envio de nuevo dato
    public function testPost()
    {
        $this->json('POST', '/api/notes', [
            'id' => '6',
            'type' => 'type3',
            'amount' => '3',
            'date' => '3',
            'category' => 'category3',
            'description' => 'descrption3',            
        ])->assertStatus(200);
        $this->json('GET', 'api/notes/6')//consultamos lo que enviamos en el post y por id ademas consultamos por id
            ->assertStatus(200)
            ->assertJson([
            'id' => '6',
            'type' => 'type3',
            'amount' => '3',
            'date' => '3',
            'category' => 'category3',
            'description' => 'descrption3',
            ]);
    }

    //test delete
    public function testDelete()
    {
        $this->json('GET', 'api/notes/1')->assertStatus(200);
        $this->json('DELETE', 'api/notes/1')->assertStatus(200);
        $this->json('GET', 'api/notes/1')->assertStatus(404);
     }

    
    
    //test colsulta por id
     public function testGetId()
    {
        $this->json('GET', 'api/notes/1')
             ->assertStatus(200)
             ->assertJson(
                 [
                    'id' => '1',
                    'type' => 'income',
                    'amount' => '1',
                    'date' => '1',
                    'category' => 'category 1',
                    'description' => 'description 1',
                 ]
             );
     }

     //Modificar dato por id
     public function testPut()
     {
         $data = [
            'id' => '1',
            'type' => 'income2',
            'amount' => '12',
            'date' => '1',
            'category' => 'category 1',
            'description' => 'description 1'
         ];

         $expected = [
            'id' => '1',
            'type' => 'income2',
            'amount' => '12',
            'date' => '1',
            'category' => 'category 1',
            'description' => 'description 1'
         ];

         $this->json('PUT', 'api/notes/1', $data)
             ->assertStatus(200)
             ->assertJson($expected);

        $this->json('GET', 'api/notes/1')
             ->assertStatus(200)
             ->assertJson($expected);
     }


     //test CONSULTA SALDO
     public function testGetBalance()
    {
        $this->json('GET', '/api/notes/get_balance/')
             ->assertStatus(200)
             ->assertJson(
                 [
                    'total' => '-3'
                  ]
             );
     }


     
    //test consulta entre fechas
    public function testGetDate()
    {
        $this->json('GET', 'api/notes/bydate?from=1&to=1')
             ->assertStatus(200)
             ->assertJson(
                 [
                    ['id' => '1',
                    'type' => 'income',
                    'amount' => '1',
                    'date' => '1',
                    'category' => 'category 1',
                    'description' => 'description 1',],
                    ['id' => '3',
                    'type' => 'expenses',
                    'amount' => '2',
                    'date' => '1',
                    'category' => 'category 2',
                    'description' => 'description 2',]
                 ]
             );
     }

     





     //TEST de validacion no existe dato
     public function testGetNotExist()
     {
        $this->json('GET', 'api/notes/98')->assertStatus(404);

        $this->json('DELETE', 'api/notes/98')->assertStatus(404);

        $this->json('PUT', 'api/comments/98', 
            [
            'id' => '98',
            'type' => 'income2',
            'amount' => '12',
            'date' => '1',
            'category' => 'category 1',
            'description' => 'description 1'
                                    ]
        )
            ->assertStatus(404);

         
     }

     











}