<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//consulta de todos los datos(todos los movimientos)
Route::get('/notes', function (Request $request) {
    $results = DB::select('select id, type, amount, date, category, description from notes ORDER BY date DESC');
    return response()->json($results, 200);
});

// envio de informacion(movimientos nuevos)
Route::post('/notes/', function() {
$data = request()->all();
DB::insert(
    "
    insert into notes (id, type, amount, date, category, description)
    values (:id, :type, :amount, :date, :category, :description)
    ",
    $data
);
$results = DB::select('select * from notes where id = :id', [
    'id' => $data['id'],
]);
return response()->json($results[0], 200);
});


//Consulta entre fecha

Route::get('/notes/bydate', function () {
        $data = request()->all();
        $initdate= $data["from"];
        $enddate= $data["to"];
        $results = DB::Select('select * from notes WHERE date BETWEEN :initdate AND :enddate',
        [
            'initdate' => $initdate,
            'enddate' => $enddate,
        ]);
        return response()->json($results, 200);
    });


    //Consulta SALDO TOTAL

Route::get('/notes/get_balance/', function (Request $request) {

    $results = DB::select('select sum(case when type="income" then amount else 0 end - case when type="expenses" then amount else 0 end) as total from notes');
    return response()->json($results[0], 200);
});


//consulta de movimiento por id
Route::get('/notes/{id}', function ($id) {
   
    if (NoExists($id)) {
        abort(404);
    }
    
    $results = DB::select('select * from notes where id=:id ', [
        'id' => $id,
    ]);

    return response()->json($results[0], 200);
});


//borrar movimiento por id
Route::delete('/notes/{id}', function ($id) {
    // Devolvemos un error con status code 404 si no hay registros en la tabla
    if (NoExists($id)) {
        abort(404);
    }

    DB::delete('delete from notes where id = :id', ['id' => $id]);

    return response()->json('', 200);
});


//Modificar dato por id
Route::put('/notes/{id}', function ($id) {
    if (NoExists($id)) {
        abort(404);
    }
    $data = request()->all();
    DB::delete(
        "
        delete from notes where id = :id",
        ['id' => $id]
    );
    DB::insert(
        "
        insert into notes (id, type, amount, date, category, description)
        values (:id, :type, :amount, :date, :category, :description)
    ",
        $data
    );
    $results = DB::select('select * from notes where id = :id', ['id' => $id]);
    return response()->json($results[0], 200);
});












//funcion de validacion no existe dato
if (!function_exists('NoExists')) {
    function NoExists($id)
    {
        $results = DB::select('select * from notes where id=:id', [
            'id' => $id,
        ]);
        return count($results) == 0;
    }
}