import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
    routes: [
        {
            path: '/main',
            component: function(resolve) {
                require(['@/components/MainPage.vue'], resolve)
            },
        },

        {
            path: '/home',
            component: function(resolve) {
                require(['@/components/HomePage.vue'], resolve)
            },
        },
        {
            path: '/income',
            component: function(resolve) {
                require(['@/components/Income/IncomePage.vue'], resolve)
            },
        },

        {
            path: '/expenses',
            component: function(resolve) {
                require(['@/components/Expenses/ExpensesPage.vue'], resolve)
            },
        },

        {
            path: '/movements',
            component: function(resolve) {
                require([
                    '@/components/Movements/MovementsListPage.vue',
                ], resolve)
            },
        },

        {
            path: '/details/:movementId',
            component: function(resolve) {
                require([
                    '@/components/Details/MovementsDetailsPage.vue',
                ], resolve)
            },
        },
    ],
})
export default router
