import { shallowMount } from '@vue/test-utils'
import IncomePage from '@/components/Income/IncomePage.vue'
import IncomeComponent from '@/components/Income/IncomeComponent.vue'



test('si funciona la function uploadNewIncome', async () => {
    const MockUploadNewIncome = jest.fn()
    const mockNewIncome = [
        {
            id: '89',
            type: 'income',
            amount: "250",
            date: "2020-04-03",
            category: "Pasivo",
            description: "nomina",

        },
    ]
    MockUploadNewIncome.mockReturnValue(mockNewIncome)

    const wrapper = shallowMount(IncomePage, {
        data() {
            return {}
        },
        methods: {
            ...IncomePage.methods,
            uploadNewIncome: MockUploadNewIncome,
        },
    })
    const newIncome = mockNewIncome

    await wrapper.vm.$nextTick()

    const income = wrapper.find(IncomeComponent)

    income.vm.$emit('upload-new-income', newIncome)

    await wrapper.vm.$nextTick()

    expect(MockUploadNewIncome).toHaveBeenCalled()
})
