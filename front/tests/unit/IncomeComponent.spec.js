import { shallowMount } from '@vue/test-utils'
import IncomeComponent from '@/components/Income/IncomeComponent.vue'

test('emite event Upload new Income ', async () => {
    const wrapper = shallowMount(IncomeComponent)
    expect(wrapper.emitted().search).toBe(undefined)

    const amount = wrapper.find('.amount')
    const date = wrapper.find('.date')

    const description = wrapper.find('.description')

    const submit = wrapper.find('.addIncome')

    amount.setValue('4545')
    date.setValue('2020-04-20')

    description.setValue("test descrip de nomina")

    submit.trigger("click");
    await wrapper.vm.$nextTick();

    expect(wrapper.emitted()["upload-new-income"].length).toBe(1);
    expect(wrapper.emitted()["upload-new-income"]).toEqual([
        [
            {
                amount: '4545',
                date: "2020-04-20",
                category: null,
                description: 'test descrip de nomina',

            }
        ]
    ]);
});

