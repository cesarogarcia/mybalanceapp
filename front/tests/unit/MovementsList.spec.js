import { shallowMount } from '@vue/test-utils'
import MovementsList from '@/components/Movements/MovementsList.vue'
import MovementsItem from '@/components/Movements/MovementsItem.vue'

test('pinta todo list vacío', () => {
    const wrapper = shallowMount(MovementsList, {
        propsData: {
            listOfMovement: [],
        },
    })
    const movementList = wrapper.findAll(MovementsItem).wrappers
    expect(movementList.length).toBe(0)
})

test('pinta todo list con movements', () => {
    const listOfMovement = [
        {
            id: '1',
            type: 'INCOME',
            category: 'Pasivo',
            description: 'NOMINA1',
            date: '2020-01-27',
        },
        {
            id: '2',
            type: 'INCOME',
            category: 'Pasivo',
            description: 'NOMINA2',
            date: '2020-02-27',
        },
        {
            id: '3',
            type: 'INCOME',
            category: 'Pasivo',
            description: 'NOMINA3',
            date: '2020-03-27',
        },
        {
            id: '4',
            type: 'INCOME',
            category: 'Pasivo',
            description: 'NOMINA3',
            date: '2020-04-27',
        },
    ]
    const wrapper = shallowMount(MovementsList, {
        propsData: {
            listOfMovement: listOfMovement,
        },
    })
    const movementsList = wrapper.findAll(MovementsItem).wrappers
    expect(movementsList.length).toBe(4)
})

test('emite evento button delete movement', async () => {
    const listOfMovement = [
        {
            id: '1',
            type: 'INCOME',
            category: 'Pasivo',
            description: 'NOMINA1',
            date: '2020-01-27',
        },
        {
            id: '2',
            type: 'INCOME',
            category: 'Pasivo',
            description: 'NOMINA2',
            date: '2020-02-27',
        },
        {
            id: '3',
            type: 'INCOME',
            category: 'Pasivo',
            description: 'NOMINA3',
            date: '2020-03-27',
        },
        {
            id: '4',
            type: 'INCOME',
            category: 'Pasivo',
            description: 'NOMINA3',
            date: '2020-04-27',
        },
    ]
    const wrapper = shallowMount(MovementsList, {
        propsData: {
            listOfMovement: listOfMovement,
        },
    })
    wrapper.vm.$emit('delete-movement-event', 2)
    /* wrapper.vm.$emit('delete-movement-event', 123)*/
    await wrapper.vm.$nextTick()
    expect(wrapper.emitted()['delete-movement-event'].length).toBe(1)
    expect(wrapper.emitted()['delete-movement-event'][0]).toEqual([2])
})
