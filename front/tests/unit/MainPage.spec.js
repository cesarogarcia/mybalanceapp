import { shallowMount } from '@vue/test-utils'
import MainPage from '@/components/MainPage.vue'
import {mockRouter} from "./helpers"


function mountPage() {return shallowMount(MainPage, {
    mocks:{$router:mockRouter}
})}


test('pinta el botón goIncome', () => {
    const wrapper = shallowMount(MainPage, {
        propsData: {
            state: true,
        },
    })
    let button = wrapper.find('.goIncome')
    expect(button.is('button')).toBe(true)
})

test('ejecuta el botón goIncome', async () => {
    const wrapper = mountPage()
    wrapper.findAll('.goIncome').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$router.path).toBe('/income')

   
})

test('pinta el botón goExpenses', () => {
    const wrapper = shallowMount(MainPage, {
        propsData: {
            state: true,
        },
    })
    let button = wrapper.find('.goExpenses')
    expect(button.is('button')).toBe(true)
})

test('ejecuta el botón goExpenses', async () => {
    const wrapper = mountPage()
    wrapper.findAll('.goExpenses').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$router.path).toBe('/expenses')

   
})

test('pinta el botón goMovements', () => {
    const wrapper = shallowMount(MainPage, {
        propsData: {
            state: true,
        },
    })
    let button = wrapper.find('.goMovements')
    expect(button.is('button')).toBe(true)
})

test('ejecuta el botón goMovements', async () => {
    const wrapper = mountPage()
    wrapper.findAll('.goMovements').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$router.path).toBe('/movements')

   
})

test('pinta el botón exitMain', () => {
    const wrapper = shallowMount(MainPage, {
        propsData: {
            state: true,
        },
    })
    let button = wrapper.find('.exitMain')
    expect(button.is('button')).toBe(true)
})

test('ejecuta el botón exitMain', async () => {
    const wrapper = mountPage()
    wrapper.findAll('.exitMain').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$router.path).toBe('/home')

   
})