import { shallowMount } from '@vue/test-utils'
import MovementsItem from '@/components/Movements/MovementsItem.vue'
import { mockRouter } from './helpers'

function mountPage() {

    return shallowMount(MovementsItem, {
        propsData: {
            movement: {
                id: '2',
                type: 'INCOME',
                category: 'Pasivo',
                description: 'NOMINA',
                date: '2020-05-27',
            },
        },
        
        mocks: { $router: mockRouter },
    })
}

test('Verifica que saca el nombre debido', () => {
    const wrapper = shallowMount(MovementsItem, {
        propsData: {
            movement: {
                id: '2',
                type: 'INCOME',
                category: 'Pasivo',
                description: 'NOMINA',
                date: '2020-05-27',
            },
        },
    })
    expect(wrapper.text()).toContain('INCOME Pasivo NOMINA 2020-05-27')
})


test('ejecuta el botón seeMovement', async () => {
    const wrapper = mountPage()
    wrapper.find('.full-name').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$router.path).toBe('/details/2')

   
})

test('ejecuta el botón editMovement', async () => {
    const wrapper = mountPage()
    wrapper.findAll('#edit').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$router.path).toBe('/details/2')

   
})

test('emite evento click deleteMovement', async () => {
    const wrapper = shallowMount(MovementsItem, {
        propsData: {
            movement: {
                id: '2',
                type: 'INCOME',
                category: 'Pasivo',
                description: 'NOMINA',
                date: '2020-05-27',
            },
        },
    })
    expect(wrapper.emitted('delete-movement-event')).toBe(undefined)

    // Simular lo que hace el usuario.
    const namej = wrapper.find('#delete')
    namej.trigger('click')
    await wrapper.vm.$nextTick()

    // Comprobar la emisión.
    expect(wrapper.emitted('delete-movement-event').length).toBe(1)
    expect(wrapper.emitted('delete-movement-event')[0]).toEqual(['2'])
})



test('Crear un Computed para meter en fullName la concatenación de name y surname.', async () => {
    const wrapper = shallowMount(MovementsItem, {
        propsData: {
            movement: {
                id: '2',
                type: 'INCOME',
                category: 'Pasivo',
                description: 'NOMINA',
                date: '2020-05-27',
            },
        },
    })
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.fullMovement).toContain('INCOME Pasivo NOMINA 2020-05-27')
})

test.skip('emite evento click seeMovement', async () => {
    const wrapper = shallowMount(MovementsItem, {
        propsData: {
            movement: {
                id: '2',
                type: 'INCOME',
                category: 'Pasivo',
                description: 'NOMINA',
                date: '2020-05-27',
            },
        },
    })
    expect(wrapper.emitted('see-movement-event')).toBe(undefined)

    // Simular lo que hace el usuario.
    const namej = wrapper.find('a')
    namej.trigger('click')
    await wrapper.vm.$nextTick()

    // Comprobar la emisión.
    expect(wrapper.emitted('see-movement-event').length).toBe(1)
    expect(wrapper.emitted('see-movement-event')[0]).toEqual(['2'])
})

test.skip('emite evento click editMovement', async () => {
    const wrapper = shallowMount(MovementsItem, {
        propsData: {
            movement: {
                id: '2',
                type: 'INCOME',
                category: 'Pasivo',
                description: 'NOMINA',
                date: '2020-05-27',
            },
        },
    })
    expect(wrapper.emitted('edit-movement-event')).toBe(undefined)

    // Simular lo que hace el usuario.
    const namej = wrapper.find('#edit')
    namej.trigger('click')
    await wrapper.vm.$nextTick()

    // Comprobar la emisión.
    expect(wrapper.emitted('edit-movement-event').length).toBe(1)
    expect(wrapper.emitted('edit-movement-event')[0]).toEqual(['2'])
})