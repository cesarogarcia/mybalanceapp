import { shallowMount } from '@vue/test-utils'
import MovementsListPage from '@/components/Movements/MovementsListPage.vue'
import MovementsList from '@/components/Movements/MovementsList.vue'
import SearchDateComponent from '@/components/Movements/SearchDateComponent.vue'

test('comprobamos que deleteEvent borra el movimiento', async () => {
    const MockApiCall = jest.fn()
    const mockContact = undefined
    MockApiCall.mockReturnValue(mockContact)
    const wrapper = shallowMount(MovementsListPage, {
        data() {
            return {
                movement: {
                    id: '2',
                    type: 'INCOME',
                    category: 'Pasivo',
                    description: 'NOMINA',
                    date: '2020-05-27',
                },
            }
        },
        methods: {
            ...MovementsListPage.methods,
            deleteMovement: MockApiCall,
            loadMovementsList: MockApiCall,
        },
    })
    await wrapper.vm.$nextTick()
    expect(MockApiCall).toHaveBeenCalled()
    expect(wrapper.vm.movement.length).toBe(undefined)
})
