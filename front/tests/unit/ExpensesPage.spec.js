import { shallowMount } from '@vue/test-utils'
import ExpensesPage from '@/components/Expenses/ExpensesPage.vue'
import ExpensesComponent from '@/components/Expenses/ExpensesComponent.vue'



test('si funciona la function uploadNewExpenses', async () => {
    const MockUploadNewExpenses = jest.fn()
    const mockNewExpenses = [
        {
            id: '89',
            type: 'expenses',
            amount: "250",
            date: "2020-04-03",
            category: "COMIDA",
            description: "cena en familia",

        },
    ]
    MockUploadNewExpenses.mockReturnValue(mockNewExpenses)

    const wrapper = shallowMount(ExpensesPage, {
        data() {
            return {}
        },
        methods: {
            ...ExpensesPage.methods,
            uploadNewExpenses: MockUploadNewExpenses,
        },
    })
    const newExpenses = mockNewExpenses

    await wrapper.vm.$nextTick()

    const expenses = wrapper.find(ExpensesComponent)

    expenses.vm.$emit('upload-new-expenses', newExpenses)

    await wrapper.vm.$nextTick()

    expect(MockUploadNewExpenses).toHaveBeenCalled()
})
