import { shallowMount } from '@vue/test-utils'
import ExpensesComponent from '@/components/Expenses/ExpensesComponent.vue'

test('emite event Upload new Expense ', async () => {
    const wrapper = shallowMount(ExpensesComponent)
    expect(wrapper.emitted().search).toBe(undefined)

    const amount = wrapper.find('.amount')
    const date = wrapper.find('.date')
    const description = wrapper.find('.description')
    const submit = wrapper.find('.addExpenses')

    amount.setValue('4545')
    date.setValue('2020-04-20')
    description.setValue("test descrip de comida")

    submit.trigger("click");
    await wrapper.vm.$nextTick();

    expect(wrapper.emitted()["upload-new-expenses"].length).toBe(1);
    expect(wrapper.emitted()["upload-new-expenses"]).toEqual([
        [
            {
                amount: '4545',
                date: "2020-04-20",
                category: null,
                description: 'test descrip de comida',
                type:"expenses",

            }
        ]
    ]);
});