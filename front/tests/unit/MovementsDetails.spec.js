import { shallowMount } from '@vue/test-utils'
import MovementsDetails from '@/components/Details/MovementsDetails.vue'
import { mockRouter } from './helpers'

function mountPage() {
    return shallowMount(MovementsDetails, {
        mocks: { $router: mockRouter },
        propsData: {
            movementDetail: {
                id: null,
                amount: null,
                type: null,
                date: null,
                description: null,
                category: null,
            },
        },
    })
}

test('pinta el botón buttonBack', () => {
    const wrapper = mountPage()
    let button = wrapper.find('.btn-back-view')
    expect(button.is('button')).toBe(true)
})
test('ejecuta el botón buttonBack', async () => {
    const wrapper = mountPage()
    wrapper.find('.btn-back-view').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$router.path).toBe('/movements')
})


test('emite evento click deleteDetails', async () => {
    const wrapper = shallowMount(MovementsDetails, {
        propsData: {
            movementDetail: {
                id: '2',
                type: 'INCOME',
                category: 'Pasivo',
                description: 'NOMINA',
                date: '2020-05-27',
            },
        },
    })
    expect(wrapper.emitted('delete-details-movement-event')).toBe(undefined)

    // Simular lo que hace el usuario.
    const namej = wrapper.find('.btn-delete')
    namej.trigger('click')
    await wrapper.vm.$nextTick()

    // Comprobar la emisión.
    expect(wrapper.emitted('delete-details-movement-event').length).toBe(1)
    expect(wrapper.emitted('delete-details-movement-event')[0]).toEqual(['2'])
})







test('Pinta los props y el watch funciona', async () => {
    const wrapper = shallowMount(MovementsDetails, {
        propsData: {

                  movementDetail:{
                id: '2',
                amount: '258',
                type: 'INCOME', 
                date: '2020-02-27',
                description: 'NOMINA2',
                category: 'INGRESOS PASIVOS',
            },

           
        },
    });
    
    await wrapper.vm.$nextTick();
    
    expect(wrapper.vm.localMovementDetail.amount).toBe('258');
    expect(wrapper.vm.localMovementDetail.type).toBe('INCOME');
    expect(wrapper.vm.localMovementDetail.date).toBe('2020-02-27');
    expect(wrapper.vm.localMovementDetail.description).toBe('NOMINA2');
    expect(wrapper.vm.localMovementDetail.category).toBe('INGRESOS PASIVOS');

})




test('Se cambia la prop SelectedMovement cuando se pinta nuevo input', async () => {
    const wrapper = shallowMount(MovementsDetails, {
        propsData: {
            movementDetail:{
                id: '1',
                amount: null,
                type: 'income',
                
                date: null,
                description: null,
                category: null,
            },
            
        },
    })    
    const amountInput = wrapper.findAll('.amount').wrappers
    amountInput[0].setValue('258')
    const dateInput = wrapper.findAll('.date').wrappers
    dateInput[0].setValue('2020-02-27')

    /*const categoryInput = wrapper.find('.category-input-i').findAll('option')
    categoryInput.at(1).setSelected()
    console.log("")*/

    /*const categoryInput = wrapper.findAll('option').at(1).trigger('change')*/

    const descriptionInput = wrapper.findAll('.description').wrappers
    descriptionInput[0].setValue('NOMINA2')


 
    
    expect(wrapper.vm.localMovementDetail.amount).toBe('258');  
    expect(wrapper.vm.localMovementDetail.date).toBe('2020-02-27');
    expect(wrapper.vm.localMovementDetail.description).toBe('NOMINA2');
    /*expect(wrapper.vm.localMovementDetail.category).toBe('INGRESOS PASIVOS');*/
})


test('emite evento click editMovement', async () => {
    const wrapper = shallowMount(MovementsDetails, {
        propsData: {
            movementDetail: {                                                                                           
                id: '2',
                amount: '123',
                type: 'income',
                date: '2020-02-27',
                description: 'NOMINA2',
                category: 'INGRESOS PASIVOS',
            },
        },
    })
    expect(wrapper.emitted('edit-details-movement-event')).toBe(undefined)

    // Simular lo que hace el usuario.
    const namej = wrapper.find('#btn-edit')
    namej.trigger('click')
    await wrapper.vm.$nextTick()

    // Comprobar la emisión.
    expect(wrapper.emitted('edit-details-movement-event').length).toBe(1)
    expect(wrapper.emitted('edit-details-movement-event')).toEqual([
        [
            {
                id: '2',
                amount: '123',
                type: 'income',
                date: '2020-02-27',
                description: 'NOMINA2',
                category: 'INGRESOS PASIVOS',

            }
        ]
    ]);

})
