import { shallowMount } from '@vue/test-utils'
import SearchDateComponent from '@/components/Movements/SearchDateComponent.vue'
import { mockRouter } from './helpers'

function mountPage() {
    return shallowMount(SearchDateComponent, {
        mocks: { $router: mockRouter },
    })
}

test('emite evento click buttonSearch', async () => {
    const wrapper = shallowMount(SearchDateComponent)
    expect(wrapper.emitted('button-search-event')).toBe(undefined)
    const dateto = wrapper.find('.date-to')
    const datefrom = wrapper.find('.date-from')
    const search = wrapper.find('.button-search')

    // Simular lo que hace el usuario.

    datefrom.setValue('2020-05-20')
    dateto.setValue('2020-05-25')
    search.trigger('click')
    await wrapper.vm.$nextTick()

    // Comprobar la emisión.
    expect(wrapper.emitted('button-search-event').length).toBe(1)
    expect(wrapper.emitted('button-search-event')).toEqual([
        ['2020-05-20', '2020-05-25'],
    ])
})

test('pinta el botón buttonBack', () => {
    const wrapper = shallowMount(SearchDateComponent, {
        propsData: {
            state: true,
        },
    })
    let button = wrapper.find('.button-back')
    expect(button.is('button')).toBe(true)
})

test('ejecuta el botón buttonBack', async () => {
    const wrapper = mountPage()
    wrapper.findAll('.button-back').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$router.path).toBe('/main')
})
